let page = new URLSearchParams(window.location.search).get('page');
document.title = '<' + page + '>';
let placeholder = document.getElementById('placeholder');
let placeholderText = document.getElementById('placeholder_text');
placeholder.addEventListener('click', onClickPlaceholder);
//placeholder.setAttribute('held_page', page);
placeholderText.innerHTML = page;

function onClickPlaceholder(clickData) {
  let placeholder = document.getElementById('placeholder');
  let placeholderText = document.getElementById('placeholder_text');
  let page = placeholderText.innerHTML;
  navigator.clipboard.writeText(page);
  placeholder.setAttribute('recently_copied', true);
//  placeholder.setAttribute('recently_copied', false);
  window.setTimeout(
    () => placeholder.setAttribute('recently_copied', false), 
    1000
  );
}